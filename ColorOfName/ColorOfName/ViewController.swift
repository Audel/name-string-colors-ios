//
//  ViewController.swift
//  ColorOfName
//
//  Created by Audel Dugarte on 5/4/18.
//  Copyright © 2018 Audel Dugarte. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nametextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var birthDatePicker: UIDatePicker!
    
    @IBOutlet weak var displayColorView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        nametextField.delegate = self
        lastNameTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func clickedGenerateColorOfName(_ sender: UIButton) {
        
        if let firstName = nametextField.text{
            
            if let lastName = lastNameTextField.text{
                //do everything
                if(firstName.isEmpty){
                    showAlertDialog(title: "missing info", message: "please write your first name")
                }else if(lastName.isEmpty){
                    showAlertDialog(title: "missing info", message: "please write your last name")
                }else{
                    NSLog("los hashes son \(firstName.hash) y \(lastName.hash) ")
                    let colorHexStrGenerated = printMyHexText(name: firstName, lastName: lastName)
                    
                    let daIndex = colorHexStrGenerated.index(colorHexStrGenerated.endIndex, offsetBy: -9)
                    let dbIndex = colorHexStrGenerated.index(colorHexStrGenerated.endIndex, offsetBy: -4)
                    let colorCompatibleHexCode = colorHexStrGenerated[daIndex...dbIndex]
                    
                    //let daColor = UIColor(hexString: "#FF1234")!
                    let daColor = UIColor(hexString: String(colorCompatibleHexCode) )!
                    
                    displayColorView.backgroundColor = daColor
                }
                
            }else{
                //error dialog
                showAlertDialog(title: "Error", message: "something went wrong")
            }
        }else{
            //error dialog
            showAlertDialog(title: "Error", message: "something went wrong")
        }
    }
    
    func printMyHexText(name : String, lastName : String)->String{
        //var baseIntA = Int(arc4random() % 65535)
        //var baseIntB = Int(arc4random() % 65535)
        
        var dateNmbr = 0
        
        if let dateNmrbrPckr = birthDatePicker{
            dateNmbr = dateNmrbrPckr.date.hashValue
        }
        
        let baseIntA = Int((name.hash + dateNmbr) % 65535)
        let baseIntB = Int((lastName.hash - dateNmbr) % 65535)
        
        //var str = String(format: "%06X%06X", baseIntA, baseIntB)
        var str = String(format: "%06X%06X", baseIntA, baseIntB)
        NSLog("generated hex is \(str)")
        
        return str
    }
    
    func showAlertDialog(title :String, message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        //alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func isOdd(number :NSInteger)-> Bool{
        return number%2 != 0;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
        
    }
}
